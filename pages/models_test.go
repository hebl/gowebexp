package pages

import (
	"strings"
	"testing"
)

func TestRenderedContent(t *testing.T) {
	p := Page{Name: "foo", Slug: "foo", Content: "# hello\n**world**"}
	rendered_content := string(p.RenderedContent())
	if strings.Contains(rendered_content, "<h1>hello</h1>") != true {
		t.Error("An error occured because # hello should be converted to a heading1")
	}
}

func TestValidatePageNoError(t *testing.T) {
	p := Page{Name: "foo", Slug: "foo", Content: "# hello\n**world**"}
	validationErrors, err := p.Validate()
	if err != nil {
		t.Error("There is a validation error on a valid page", err, validationErrors)
	}
}

func TestValidatePageErrorEmptyName(t *testing.T) {
	p := Page{Name: "", Slug: "foo", Content: "# hello\n**world**"}
	validationErrors, err := p.Validate()

	if err != nil && validationErrors["Name"] != "This field is required" {
		t.Error("There is an expected validation error and the message is not appropriate", err, validationErrors["Name"])
	}
}

func TestValidatePageErrorEmptySlug(t *testing.T) {
	p := Page{Name: "foo", Slug: "", Content: "# hello\n**world**"}
	validationErrors, err := p.Validate()

	if err != nil && validationErrors["Slug"] != "This field is required" {
		t.Error("There is an expected validation error and the message is not appropriate", err, validationErrors["Name"])
	}
}
