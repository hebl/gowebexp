package main

import (
	"bitbucket.org/yml/gowebexp/web"
	"log"
	"net/http"
)

const (
	ADDRESS = ":8080"
)

func main() {

	web.App.SetRoute()
	// Start serving
	log.Println("Starting server on: ", ADDRESS)
	http.Handle("/", web.App)

	// Serving static file
	http.Handle("/static/",
		http.StripPrefix("/static/", http.FileServer(http.Dir(web.App.StaticDir))))

	err := http.ListenAndServe(ADDRESS, nil)
	if err != nil {
		log.Fatal("An error occured when trying to server gowebexp: \n", err)
	}
}
